﻿using Credit_Card.Controllers;
using Credit_Card.Models;
using Credit_Card.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Results;
using System.Data.SqlClient;

namespace Credit_Card.Tests
{
    [TestClass]
    public class CardControllerTest
    {
           
        [TestMethod]
        public void Post_Visa_Return_BadRequestStatusCode()
        {

            var controller = new CardController();

            controller.Request = new HttpRequestMessage();
            controller.Request.SetConfiguration(new HttpConfiguration());
            var card = new Card
            {

                CardNumber = "4200000000000000",
                ExpiryDate = "012018"
            };

            var result = controller.Post(card);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
        }
        [TestMethod]
        public void Post_Visa_Return_OKStatusCode_LeapYear()
        {

            var controller = new CardController();
            controller.Request = new HttpRequestMessage();
            controller.Request.SetConfiguration(new HttpConfiguration());
           
            var card = new Card
            {

                CardNumber = "4200000000000000",
                ExpiryDate = "022020"
            };

            var result = controller.Post(card);
            Assert.AreEqual(HttpStatusCode.Created, result.StatusCode);
        }
        [TestMethod]
        public void Post_Master_Return_BadRequestStatusCode()
        {
            var controller = new CardController();
            controller.Request = new HttpRequestMessage();
            controller.Request.SetConfiguration(new HttpConfiguration());
            var card = new Card
            {

                CardNumber = "5100000000000000",
                ExpiryDate = "052020"
            };

            var result = controller.Post(card);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
        }
        [TestMethod]
        public void Post_Master_Return_OKStatusCode_Primenumber()
        {
            var controller = new CardController();
            controller.Request = new HttpRequestMessage();
            controller.Request.SetConfiguration(new HttpConfiguration());
            var card = new Card
            {
                CardNumber = "5100000000000000",
                ExpiryDate = "022027"
            };

            var result = controller.Post(card);
            Assert.AreEqual(HttpStatusCode.Created, result.StatusCode);
        }

        [TestMethod]
        public void Post_Amex_Return_OKStatusCode()
        {
            var controller = new CardController();
            controller.Request = new HttpRequestMessage();
            controller.Request.SetConfiguration(new HttpConfiguration());
            var card = new Card
            {
                CardNumber = "320000000000000",
                ExpiryDate = "022019"
            };

            var result = controller.Post(card);
            Assert.AreEqual(HttpStatusCode.Created, result.StatusCode);
        }
        [TestMethod]
        public void Post_JCB_Return_OKStatusCode()
        {
            var controller = new CardController();
            controller.Request = new HttpRequestMessage();
            controller.Request.SetConfiguration(new HttpConfiguration());
            var card = new Card
            {
                CardNumber = "3100000000000000",
                ExpiryDate = "122018"
            };

            var result = controller.Post(card);
            Assert.AreEqual(HttpStatusCode.Created, result.StatusCode);
        }
        [TestMethod]
        public void Post_Visa_Return_BadRequestStatusCode_Doesnotexist()
        {
            var controller = new CardController();
            controller.Request = new HttpRequestMessage();
            controller.Request.SetConfiguration(new HttpConfiguration());
            var card = new Card
            {
                CardNumber = "4300000000000000",
                ExpiryDate = "052020"
            };

            var result = controller.Post(card);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
        }
        [TestMethod]
        public void Post_UnKnown_Return_BadRequestStatusCode()
        {
            var controller = new CardController();
            controller.Request = new HttpRequestMessage();
            controller.Request.SetConfiguration(new HttpConfiguration());
            var card = new Card
            {
                CardNumber = "120000000000000",
                ExpiryDate = "052020"
            };

            var result = controller.Post(card);
            Assert.AreEqual(HttpStatusCode.BadRequest, result.StatusCode);
        }
    }
}
