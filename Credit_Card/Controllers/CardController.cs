﻿using Credit_Card.Models;
using Credit_Card.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;


namespace Credit_Card.Controllers
{
    public class CardController : ApiController
    {
       
        private CardRepository cardRepository;

        public CardController()
        {
            this.cardRepository = new CardRepository();
        }

        public HttpResponseMessage Post(Card card)
        {
            ValidateCard(card);
            if (ModelState.IsValid)
            {
                /*return new HttpResponseMessage(HttpStatusCode.OK);*/
                var response = Request.CreateResponse<Card>(System.Net.HttpStatusCode.Created, card);

                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }
        private void ValidateCard(Card card)
        {
            if (string.IsNullOrEmpty(card.CardNumber))
            {
                ModelState.AddModelError("Invalid", "Please input card number.");
            }
            if (!string.IsNullOrEmpty(card.CardNumber) )
            {
                if (card.CardNumber.Length < 15)
                {
                    ModelState.AddModelError("Invalid", "Card number is invalid.");
                }
            }
            if (string.IsNullOrEmpty(card.ExpiryDate))
            {
                ModelState.AddModelError("Invalid", "Please input expiry date.");
            }
            if (!string.IsNullOrEmpty(card.ExpiryDate))
            {
                var expiry = card.ExpiryDate;
                if (expiry.Length != 6)
                {
                    ModelState.AddModelError("Invalid", "Please input expiry date.");
                }
                else
                {
                    int month = int.Parse(card.ExpiryDate.Substring(0, 2));
                    if (month < 0 || month > 12)
                    {
                        ModelState.AddModelError("Invalid", "Please input valid month.");
                    }

                    int year = int.Parse(card.ExpiryDate.Substring(2, 4));
                    if (year == DateTime.Now.Year)
                    {
                        if (month <= DateTime.Now.Month)
                        {
                            ModelState.AddModelError("Invalid", "Please input valid month.");
                        }
                    }
                    else if (year < DateTime.Now.Year)
                    {
                        ModelState.AddModelError("Invalid", "Please input valid year.");
                    }



                }
            }


            if (ModelState.IsValid)
            {
                //var cardType = cardTypeRepository.GetCardTypes().SingleOrDefault(p => p.CardTypeID == card.CardTypeID);
                //if (cardType != null)
                //{
                string startNumber = card.CardNumber.Substring(0, 1);
                int lengthNumber = card.CardNumber.Length;
                var expiry = card.ExpiryDate.Substring(2, 4);
                int expiryYear = int.Parse(expiry);

                if (startNumber == "4" && lengthNumber == 16)
                {
                    if (!CheckLeapYear(expiryYear))
                    {
                        ModelState.AddModelError("Invalid", "Visa card's expiry date does not match.");
                    }
                }
                if (startNumber == "5" && lengthNumber == 16)
                {
                    if (!CheckPrimeNumber(expiryYear, 2))
                    {
                        ModelState.AddModelError("Invalid", "Master card's expiry date does not match.");
                    }
                }
                if (startNumber != "3" && startNumber != "4" && startNumber != "5")
                {
                    ModelState.AddModelError("Invalid", "Card number is invalid.");
                }
                if (!cardRepository.CheckCardNumber(card.CardNumber))
                {
                    ModelState.AddModelError("Invalid", "Card number does not exist.");
                }
            }



        }
        private bool CheckLeapYear(int year)
        {
            if ((year % 4) == 0)
            {
                if ((year % 100) == 0)
                {
                    return ((year % 400) == 0);
                }
                return true;
            }
            return false;

        }
        private bool CheckPrimeNumber(int year, int i)
        {
            if ((year % i) == 0)
            {
                if (year == i)
                {
                    return true;
                }
                return false;
            }
            else
            {
                if (year > i)
                {
                    return CheckPrimeNumber(year, i + 1);
                }
                return false;
            }
        }


    }
}
