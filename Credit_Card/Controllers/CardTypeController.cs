﻿using Credit_Card.Models;
using Credit_Card.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Credit_Card.Controllers
{
    public class CardTypeController : ApiController
    {
       private CardTypeRepository cardTypeRepository;

        public CardTypeController()
        {
            this.cardTypeRepository = new CardTypeRepository();
        }
        public IEnumerable<CardType> GetAllCardTypes()
        {
            return cardTypeRepository.GetCardTypes();
        }
    
    }
}
