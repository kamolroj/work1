﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Credit_Card.Models
{
    public class CardType
    {
        public int CardTypeID { get; set; }
        public string CardTypeName { get; set; }
        public int digits { get; set; }
        public int StartNumber { get; set; }
    }
}