﻿using Credit_Card.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Credit_Card.Service
{
    public class CardRepository
    {
        public bool CheckCardNumber(string cardNumber)
        {
            PaymentEntities _db = new PaymentEntities();
            int? count = _db.CheckCardNumber(cardNumber).SingleOrDefault();
            return (count ?? 0) > 0;
        }
    }
}