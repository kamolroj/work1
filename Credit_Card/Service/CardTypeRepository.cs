﻿using Credit_Card.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Credit_Card.Service
{
    public class CardTypeRepository
    {
        public CardType[] GetCardTypes()
        {
            CardType[] cardTypes = new CardType[]
           {
            new CardType { CardTypeID = 1, CardTypeName = "Visa", digits = 16, StartNumber = 4 },
            new CardType { CardTypeID = 2, CardTypeName = "Master", digits = 16, StartNumber = 5 },
            new CardType { CardTypeID = 3, CardTypeName = "Amex", digits = 15, StartNumber = 3 },
            new CardType { CardTypeID = 4, CardTypeName = "JCB or Unknown", digits = 16, StartNumber = 3 }
          };
            return cardTypes;
        }
    }
}